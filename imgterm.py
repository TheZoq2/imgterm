from base64 import encodebytes
from IPython.lib.latextools import latex_to_png_dvipng


def mathcat(data, meta):
    png = latex_to_png_dvipng(f'$${data}$$'.replace('\\displaystyle', '').replace('$$$', '$$'), wrap=False, color="White")
    imcat(png)

IMAGE_CODE = '\033]1337;File=name=name;inline=true;:{}\a'

def imcat(image_data):
    print(IMAGE_CODE.format(encodebytes(image_data).decode()))

def register_mimerenderer(ipython, mime, handler):
    ipython.display_formatter.active_types.append(mime)
    ipython.display_formatter.formatters[mime].enabled = True
    ipython.mime_renderers[mime] = handler

def load_ipython_extension(ipython):
    register_mimerenderer(ipython, 'image/png', imcat)
    register_mimerenderer(ipython, 'image/jpeg', imcat)
    register_mimerenderer(ipython, 'text/latex', mathcat)
